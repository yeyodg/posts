import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { PostsServiceProvider } from '../../providers/posts-service/posts-service';

/**
 * Generated class for the ItemDeletePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-item-delete',
  templateUrl: 'item-delete.html',
})
export class ItemDeletePage {
  post: any;
  constructor(
    public postsServiceProvider: PostsServiceProvider,
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams) {
    this.post = navParams.get('post');

  }
  /* 
   * Llama a la funcion de eliminar post en le servicio de posts, muestra por
   * consola la respuesta de JSONPlaceholder
   */
  deletePost() {
    this.postsServiceProvider.deletePost(this.post.id)
      .subscribe(
        (data) => console.log(data),
        (error) => console.log(error)
      );
  }
}
