import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemDeletePage } from './item-delete';

@NgModule({
  declarations: [
    ItemDeletePage,
  ],
  imports: [
    IonicPageModule.forChild(ItemDeletePage),
  ],
})
export class ItemDeletePageModule {}
