import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PostsServiceProvider } from '../../providers/posts-service/posts-service';


/**
 * Generated class for the ItemEditPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()

@Component({
  selector: 'page-item-edit',
  templateUrl: 'item-edit.html',
})
export class ItemEditPage {
  post: any;
  form: FormGroup;
  constructor(
    public postServiceProvider: PostsServiceProvider,
    public navCtrl: NavController,
    formBuilder: FormBuilder,
    public viewCtrl: ViewController,
    public navParams: NavParams) {
    this.post = navParams.get('post');
    this.form = formBuilder.group({
      title: [this.post.title, Validators.required],
      body: [this.post.body]
    });
  }
  /* 
   * Llama a la funcion para editar el post, luego muestra la respuetsa de la
   * API por consola 
   */
  editPost() {
    this.post.title = this.form.value.title;
    this.post.body = this.form.value.body;
    return this.postServiceProvider.editPost(this.post)
      .subscribe(
        (data) => console.log(data),
        (error) => console.log(error)
      )
  }
  cancel() {
    this.viewCtrl.dismiss();
  }

}
