import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { PostsServiceProvider } from '../../providers/posts-service/posts-service';

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html'
})
export class SearchPage {

  currentItems: any = [];
  posts: any;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public postServiceProvider: PostsServiceProvider
    ) { }

  /**
   * Perform a service for the proper items.
   */
  getItems(ev) {
    let val = ev.target.value;
    if (!val || !val.trim()) {
      this.currentItems = [];
      return;
    }
    this.postServiceProvider.getPostsUser(val)
    .subscribe(
      (data) => {
        this.posts = data
        console.log(this.posts);
      },
      (error) => console.log(error)
    );
  }

  /**
   * Navigate to the detail page for this item.
   */
  openPost(id: Number) {
    this.navCtrl.push('ItemDetailPage', {
      id: id
    });
  }
  /**
 * Navigate to the detail page for this item.
 */
  editPost(post: any) {
    this.navCtrl.push('ItemEditPage', {
      post: post
    });
  }
  deletePost(post: any) {
    this.navCtrl.push('ItemDeletePage', {
      post: post
    });
  }


}
