import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, ViewController } from 'ionic-angular';
import { PostsServiceProvider } from '../../providers/posts-service/posts-service';

@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    formBuilder: FormBuilder,
    public postServiceProvider: PostsServiceProvider) {
    this.form = formBuilder.group({
      title: ['', Validators.required],
      body: ['']
    });

    // Se dispara si hay cambios en el formulario
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }
  /**
   * Cancelar la operacion
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * El usuario termino de crear el post y se envia a la API
   * muestra por consola la respuesta de la API
   */
  done() {
    if (!this.form.valid) { return; }
    this.postServiceProvider.createPost(this.form.value.title, this.form.value.body)
      .subscribe(
        (data) => {
          console.log(data);
        },
        (error) => console.log(error)
      );
  }
}
