import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PostsServiceProvider } from '../../providers/posts-service/posts-service';
import { identifierModuleUrl } from '@angular/compiler';


@IonicPage()
@Component({
  selector: 'page-item-detail',
  templateUrl: 'item-detail.html'
})
export class ItemDetailPage {
  item: any;
  post: any;
  comments: any;
  id: Number;
  constructor(
    public navCtrl: NavController,
    navParams: NavParams,
    public postsServiceProvider: PostsServiceProvider
  ) {
    this.id = navParams.get('id');
  }
  identifierModuleUrl
  /* 
   * Llama a la funcion para obtener el post, luego llama a la funcion para 
   * obtener los comentarios
   */
  ionViewDidLoad() {
    this.postsServiceProvider.getPost(this.id)
      .subscribe(
        (data) => {
          this.post = data;
          console.log(data);
          this.postsServiceProvider.getCommentPost(this.id)
            .subscribe(
              (comments) => {
                this.comments = comments;
              },
              (error) => {
                console.error(error);
              }
            );
        },
        (error) => {
          console.error(error);
        }
      );
  }

}
