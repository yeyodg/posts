import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';
import { PostsServiceProvider } from '../../providers/posts-service/posts-service';

import { Item } from '../../models/item';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Item[];
  posts: any;
  constructor(
    public navCtrl: NavController,
    public postsServiceProvider: PostsServiceProvider,
    public modalCtrl: ModalController) {
  }
  /** 
   * Se usa para ver el detalle de un post y sus comentarios, hace las llamadas
   * a la API
   */
  openPost(id: Number) {
    this.navCtrl.push('ItemDetailPage', { id: id });
  }
  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    this.postsServiceProvider.getPosts()
      .subscribe(
        (data) => {
          this.posts = data;
        },
        (error) => {
          console.error(error);
        }
      );
  }
  /**
   * Abre el Modal para crear un nuevo post
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
      }
    })
    addModal.present();
  }

  /**
   * Navega hasta el detalle del post seleccionado
   */
  editPost(post: any) {
    this.navCtrl.push('ItemEditPage', {
      post: post
    });
  }
  /**
   * Navega hasta la pagina para elimnar el post seleccionado
   */
  deletePost(post: any) {
    this.navCtrl.push('ItemDeletePage', {
      post: post
    });
  }
}
