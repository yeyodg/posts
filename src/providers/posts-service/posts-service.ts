import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the PostsServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostsServiceProvider {
  headers = {
    headers: new HttpHeaders({
      "Content-type": "application/json; charset=UTF-8"
    })
  }
  constructor(public http: HttpClient) { }
  /* 
   * Obtener todos los posts
   */
  getPosts() {
    return this.http.get('https://jsonplaceholder.typicode.com/posts');
  }
  /* 
   * Obtener un post con ID 
   */
  getPost(id: Number) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/' + id);
  }
  /* 
   * Obtener los comentarios de un post con ID
   */
  getCommentPost(id: Number) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts/' + id + '/comments');

  }
  /* 
   * Crea un nuevo post
   */
  createPost(title: String, body: String) {
    let post = {
      body: body,
      title: title,
      userId: 1
    }

    return this.http.post('https://jsonplaceholder.typicode.com/posts', post, this.headers);
  }

  /* 
 * Obtener los comentarios de un post con ID
 */
  getPostsUser(id: Number) {
    return this.http.get('https://jsonplaceholder.typicode.com/posts?userId=' + id);
  }

  /* 
  * Editar un post
  */
  editPost(post: any) {
    return this.http.put('https://jsonplaceholder.typicode.com/posts/' + post.id, post, this.headers);
  }
  /* 
   * Borrar un post
   */
  deletePost(id: Number) {
    return this.http.delete('https://jsonplaceholder.typicode.com/posts/' + id);

  }
}
