# Posts Ionic

Este proyecto es una demostracion del uso de la API JSONPlaceholder hecha con Ionic

## Tabl de contenido

1. [Instalacion](#instalacion)
2. [Paginas](#paginas)

## <a name="instalacion"></a>Instalacion

1. Descargar el proyecto
2. Descomprimir en la carpeta deseada
3. Abrir una terminal en la carpeta del proyecto
4. Instalar los paquetes necesarios con el comando
```
npm install
```
5. Correr Ionic
```
ionic serve
```


## <a name="paginas"></a>Paginas

La pagina principal carga los post que obtiene de llamar a la API. Al tocar un
post se muestra el detalle del post ademas de los comentarios para editar o 
eliminar un post hay que deslizar sobre él, en las paginas de editar y eliminar
post la app imprime por consola la respuesta de la API para demostrarque se 
hizo la llamada. Para crear un post se debe usar el boton "+" en la esquina 
superior derecha de la pagina principal, finalmete en buscarse escribe un 
numero que indicar el ID de un usuario y hace la petición a la API que retorna 
los post de un solo ID.